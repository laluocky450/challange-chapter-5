# Challange Chapter 5

# Name : Lalu Ocky Saktiya Luhung
# Full Stack Web - 8

# STEP TO OPEN WEB PAGE

# Clone repository 
First, clone the repository contained in the New Chapter 1 branch into the terminal with the git command. so that the folders contained in gitlab can be accessed on your computer by cloning them.

# Open clone folder use code editor
Then open the cloned folder with the code editor you have so you can run the code contained in the folder, namely a file with the name index.html. Then by opening the index.html file, you will be able to see how the web page looks.

# Link ERD (Entity Relation Diagram) 
https://dbdiagram.io/d/6262ce711072ae0b6ad1752b