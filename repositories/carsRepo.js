const { Cars } = require("../models");

class CarsRepository {
  static async getAll() {
    const getCars = Cars.findAll();

    return getCars;
  }

  static async create({ name, price, size, photo }) {
    const createdCar = Cars.create({
      name,
      price,
      size,
      photo,
    });

    return createdCar;
  }

  static async update({ id, name, price, size, photo }) {
    const Id = {
      where: { id },
    };
    const updatedCar = Cars.update(
      {
        name,
        price,
        size,
        photo,
      },
      Id
    );

    return updatedCar;
  }

  static async getById({ id }) {
    const getByCarsId = await Cars.findOne({
      where: {
        id,
      },
    });
    return getByCarsId;
  }


  static async deleteCar({ id }) {
    const deletedByCarId = Cars.destroy({
      where: { id },
    });

    return deletedByCarId;
  }
}


module.exports = CarsRepository;
